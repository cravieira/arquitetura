#!/usr/bin/python3

import sys
import numpy as np

DATASET_M = 8
DATASET_N = 5
DATASET_K = 7

def init_matrices(M, N, K):
    A = np.zeros(shape=(M, N), dtype=np.int64)
    B = np.zeros(shape=(N, K), dtype=np.int64)

    for i in range(DATASET_M):
        for j in range(DATASET_N):
            A[i][j] = i

    for i in range(DATASET_N):
        for j in range(DATASET_K):
            B[i][j] = j

    return A, B

def print_header(f):
    datasetm_define = "#define M {}\n".format(DATASET_M)
    datasetn_define = "#define N {}\n".format(DATASET_N)
    datasetk_define = "#define K {}\n".format(DATASET_K)
    f.writelines([datasetm_define, datasetn_define, datasetk_define])
    f.write("\n")

def print_matrix(f, mat, name):
    M = len(mat)
    N = len(mat[0])
    f.write("uint64_t {}[{}][{}] = {{\n".format(name, M, N))
    for i in range(M):
        f.write("{")
        for j in range(N):
            if j < N-1:
                f.write("{}, ".format(mat[i][j]))
            else:
                f.write("{}".format(mat[i][j]))
        if i < M-1:
            f.write("},\n")
        else:
            f.write("}\n")
    f.write("};\n\n")

if len(sys.argv) < 4:
    print("At least three arguments must be provided: M, N, and K",
          file=sys.stderr)
    sys.exit(1)
else:
    DATASET_M = int(sys.argv[1])
    DATASET_N = int(sys.argv[2])
    DATASET_K = int(sys.argv[3])

with open("dataset.h", "w") as f:
    A, B = init_matrices(DATASET_M, DATASET_N, DATASET_K)
    REF = np.matmul(A, B)
    print_header(f)
    print_matrix(f, A, "A")
    print_matrix(f, np.transpose(B), "B")
    print_matrix(f, REF, "REF")
    f.write("uint64_t C[M][K];\n")
