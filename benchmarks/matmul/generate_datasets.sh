#!/bin/bash

set -e

mkdir -p datasets

SIZES="100
       250
       500
       1000"

for i in $SIZES; do
    echo $i
    ./dataset.py $i $i $i
    mv dataset.h datasets/$i.h
done

