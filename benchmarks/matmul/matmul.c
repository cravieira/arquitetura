#include <stdio.h>
#include <stdint.h>
#include <assert.h>

#include "dataset.h"
#include "m5ops.h"

int main() {
    uint64_t i, j, k;

    printf("Benchmark start\n");
    m5_resetstats();
#ifdef NO_CHUNK
    for (i = 0; i < M; i++) {
        for (j = 0; j < K; j++) {
            C[i][j] = 0;
            for (k = 0; k < N; k++) {
                C[i][j] += A[i][k] * B[j][k];
            }
        }
    }
#else
    // First try. Not good at all!
    //for (i = 0; i < M; i++) {
    //    for (j = 0; j < K; j++) {
    //        C[i][j] = 0;
    //        uint64_t aux = 0;
    //        for (k = 0; k < N; k++) {
    //            uint64_t a_elem;
    //            if (aux == 0) {
    //                asm volatile
    //                (
    //                    "ldchunk %[b], 100(%[a])"
    //                    : [b] "=r" (a_elem)
    //                    : [a] "r" (&A[i][k])
    //                );
    //            }
    //            else {
    //                a_elem = A[i][k];
    //            }
    //            aux = aux == 99 ? 0 : aux+1;
    //            C[i][j] += a_elem * B[k][j];
    //        }
    //    }
    //}
    // Second try. At least a better result
    for (i = 0; i < M; i++) {
        uint64_t a_elem;
        for (j = 0; j < K; j++) {
            C[i][j] = 0;
            uint64_t b_elem;
            asm volatile
            (
                "ldchunk %[b], 100(%[a])"
                : [b] "=r" (b_elem)
                : [a] "r" (&B[j][0])
            );
            for (k = 0; k < N; k++) {
                b_elem = B[j][k];
                C[i][j] += A[i][k] * b_elem;
            }
        }
    }
    // Third try. Horrible performance!
    //for (i = 0; i < M; i++) {
    //    for (j = 0; j < K; j++) {
    //        C[i][j] = 0;
    //        uint64_t aux = 0;
    //        for (k = 0; k < N; k++) {
    //            uint64_t b_elem;
    //            if (aux == 0) {
    //                asm volatile
    //                (
    //                    "ldchunk %[b], 100(%[a])"
    //                    : [b] "=r" (b_elem)
    //                    : [a] "r" (&B[j][0])
    //                );
    //            }
    //            else {
    //                b_elem = B[j][k];
    //            }
    //            aux = aux == 99 ? 0 : aux+1;
    //            C[i][j] += A[i][k] * b_elem;
    //        }
    //    }
    //}
#endif
    m5_dumpstats();
    printf("Comparing result to reference...\n");
    for (i = 0; i < M; i++) {
        for (j = 0; j < K; j++) {
            assert(C[i][j] == REF[i][j]);
        }
    }
    printf("Success!\n");

    return 0;
}
