#!/bin/bash

set -ex

BENCH=matmul

mkdir -p bin/prefetch
mkdir -p bin/normal
mkdir -p dump/prefetch
mkdir -p dump/normal

for i in $(ls datasets); do
    size=$(basename -s .h $i)
    cp datasets/$i dataset.h
    make
    mv $BENCH bin/prefetch/$size
    mv $BENCH.dump dump/prefetch/$size.dump
done

for i in $(ls datasets); do
    size=$(basename -s .h $i)
    cp datasets/$i dataset.h
    make DEFINE="-D NO_CHUNK"
    mv $BENCH bin/normal/$size
    mv $BENCH.dump dump/normal/$size.dump
done

