#!/bin/bash

set -e

mkdir -p datasets

MAX_PROCS=4

SIZES="100
       1000
       10000
       100000"

PADDING="0
         2
         4
         6"

work=""
for p in $PADDING; do
    for i in $SIZES; do
        work+="./dataset.py --size $i --padding $p --file datasets/s$i-p$p.hh\n"
    done
done

echo -e $work | parallel -j$MAX_PROCS --halt now,fail=1 "{}"
