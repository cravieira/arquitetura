#include <iostream>
#include <stddef.h>
#include <assert.h>

#include "list-dataset.hh"
#include "m5ops.h"

class List
{
    private:
        ListEntry* list;
    public:
        List(ListEntry *list);
        ListEntry* find(long int x);
        void assert_list();
};

List::List(ListEntry *list) {
    this->list = list;
}

ListEntry* List::find(long int x) {
    long int check = 0;
    ListEntry* p = this->list;

#ifdef NO_PREFETCH
    // Normal
    while (p != NULL) {
        long int key = p->key;
        assert(key == check);
        check++;
        if (p->key == x)
            break;
        p = p->next;
    }
#else
    // Prefetch form
    while (p != NULL) {
        long int key;
        asm volatile (
        "ldcp %[dest], %[off](%[addr])" : [dest] "=r" (key)
                                        : [off] "I" (offsetof(ListEntry, key)),
                                          [addr] "r" (p)
        );
        assert(key == check);
        check++;
        if (key == x)
            break;
        p = p->next;
    }
#endif
    return p;
}

void List::assert_list() {
    long int check = 0;
    long int x = -1;
    ListEntry* p = this->list;

    while (p != NULL) {
        long int key = p->key;
        assert(key == check);
        check++;
        if (p->key == x)
            break;
        p = p->next;
    }

    std::cout << "Check: " << check << std::endl;
    assert(check == DATASET_SIZE);
}

int main() {
    std::cout << "Starting benchmark with list size " << DATASET_SIZE <<
        "and struct size of " << sizeof(ListEntry) << "\n";
    List l(generateDataset());
    m5_resetstats();
    ListEntry* e = l.find(DATASET_SIZE);
    m5_dumpstats();
    //std::cout << "Asserting list\n";
    //l.assert_list();
    return 0;
}
