#!/bin/bash

set -e

BENCH=linked-list

mkdir -p bin/prefetch
mkdir -p bin/no-prefetch
mkdir -p dump/prefetch
mkdir -p dump/no-prefetch

for i in $(ls datasets); do
    size=$(basename -s .hh $i)
    cp datasets/$i dataset.hh
    make
    mv $BENCH bin/prefetch/$size
    mv $BENCH.dump dump/prefetch/$size.dump
done

for i in $(ls datasets); do
    size=$(basename -s .hh $i)
    cp datasets/$i dataset.hh
    make DEFINE="-D NO_PREFETCH"
    mv $BENCH bin/no-prefetch/$size
    mv $BENCH.dump dump/no-prefetch/$size.dump
done
