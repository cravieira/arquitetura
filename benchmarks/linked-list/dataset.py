#!/usr/bin/python3

import sys
import argparse
import numpy as np
from sklearn.utils import shuffle

DATASET_SIZE = 5

def print_header(f, args):
    struct = """
typedef struct ListEntry
{
    ListEntry* next;\n"""
    for p in range(args.padding):
        struct += "    long int pad{};\n".format(p)
    struct += "    long int key;\n} ListEntry;\n"

    dataset_define = "#define DATASET_SIZE {}\n".format(DATASET_SIZE)
    dataset_begin = "ListEntry a[DATASET_SIZE] = {\n"

    to_file = [
            struct,
            dataset_define,
            dataset_begin]

    f.writelines(to_file)

def print_tail(f, start):
    start_line = "ListEntry *start = &a[{}];\n".format(start)
    f.write(start_line)

def generate_dataset():
    keys = [i for i in range(DATASET_SIZE)]
    #keys = np.arange(0, DATASET_SIZE)
    keys_shuffled = shuffle(keys, random_state=0)
    next_ptrs = [None] * DATASET_SIZE

    for i in range(len(next_ptrs)):
        key = keys_shuffled[i]
        if key == DATASET_SIZE-1:
            next_ptrs[i] = "NULL"
        else:
            target = key+1
            for j in range(len(keys)):
                if keys_shuffled[j] == target:
                    next_ptrs[i] = j
                    break

    return keys_shuffled, next_ptrs

def main(args):
    keys_s, nexts_s = generate_dataset()
    with open(args.file, "w") as f:
        pad_string = ", 0" * args.padding
        print_header(f, args)
        for i in range(len(keys_s)):
            next_ptr = nexts_s[i]
            if (next_ptr == "NULL"):
                f.write("{ NULL"+pad_string+", {} }}".format(keys_s[i]))
            else:
                f.write("{{ &a[{}]".format(next_ptr)+pad_string+", {} }}".format(keys_s[i]))
            if i != DATASET_SIZE-1:
                f.write(",\n")
            else:
                f.write("\n};\n")
        print_tail(f, keys_s.index(0))

parser = argparse.ArgumentParser(description="Generate linked-list datasets")
parser.add_argument("--padding", default=0, type=int, help="String padding")
parser.add_argument("--size", default=10, type=int, help="List size")
parser.add_argument("--file", default="dataset.hh", help="Output file")
args = parser.parse_args()

DATASET_SIZE = args.size
main(args)
