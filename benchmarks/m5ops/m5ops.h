#ifndef M5OPS_H
#define M5OPS_H

#ifdef __riscv
#define M5OP_ARM                0x00
#define M5OP_QUIESCE            0x01
#define M5OP_QUIESCE_NS         0x02
#define M5OP_QUIESCE_CYCLE      0x03
#define M5OP_QUIESCE_TIME       0x04
#define M5OP_RPNS               0x07
#define M5OP_WAKE_CPU           0x09
#define M5OP_DEPRECATED1        0x10 // obsolete ivlb
#define M5OP_DEPRECATED2        0x11 // obsolete ivle
#define M5OP_DEPRECATED3        0x20 // deprecated exit function
#define M5OP_EXIT               0x21
#define M5OP_FAIL               0x22
#define M5OP_SUM                0x23 // For testing
#define M5OP_INIT_PARAM         0x30
#define M5OP_LOAD_SYMBOL        0x31
#define M5OP_RESET_STATS        0x40
#define M5OP_DUMP_STATS         0x41
#define M5OP_DUMP_RESET_STATS   0x42
#define M5OP_CHECKPOINT         0x43
#define M5OP_WRITE_FILE         0x4F
#define M5OP_READ_FILE          0x50
#define M5OP_DEBUG_BREAK        0x51
#define M5OP_SWITCH_CPU         0x52
#define M5OP_ADD_SYMBOL         0x53
#define M5OP_PANIC              0x54

#define M5OP_RESERVED1          0x55 // Reserved for user, used to be annotate
#define M5OP_RESERVED2          0x56 // Reserved for user
#define M5OP_RESERVED3          0x57 // Reserved for user
#define M5OP_RESERVED4          0x58 // Reserved for user
#define M5OP_RESERVED5          0x59 // Reserved for user

#define M5OP_WORK_BEGIN         0x5a
#define M5OP_WORK_END           0x5b

#define M5OP_SE_SYSCALL         0x60
#define M5OP_SE_PAGE_FAULT      0x61
#define M5OP_DIST_TOGGLE_SYNC   0x62

#define STR(x) #x
#define TO_STRING(s) STR(s)

static void m5_checkpoint(void)
{
    __asm__ __volatile__ ("mv a0, zero; .long 0x0000007B | (" TO_STRING(M5OP_CHECKPOINT) " << 25);");
};
static void m5_dumpstats(void)
{
	__asm__ __volatile__ ("mv a0, zero; .long 0x0000007B | (" TO_STRING(M5OP_DUMP_STATS) " << 25);");
};
static void m5_dumpresetstats(void)
{
	__asm__ __volatile__ ("mv a0, zero; .long 0x0000007B | (" TO_STRING(M5OP_DUMP_RESET_STATS) " << 25);");
};
static void m5_exit()
{
	__asm__ __volatile__ ("mv a0, zero; .long 0x0000007B | (" TO_STRING(M5OP_EXIT) " << 25);");
};
static void m5_fail_1(void)
{
    __asm__ __volatile__ ("mv a0, zero; .long 0x0000007B | (" TO_STRING(M5OP_FAIL) " << 25);");
};
static void m5_resetstats(void)
{
    __asm__ __volatile__ ("mv a0, zero; .long 0x0000007B | (" TO_STRING(M5OP_RESET_STATS) " << 25);");
};
#else
static void m5_checkpoint(void) {};
static void m5_dumpstats(void) {};
static void m5_exit(void) {};
static void m5_fail_1(void) {};
static void m5_resetstats(void) {};
#endif

#endif
