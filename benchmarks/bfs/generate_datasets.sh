#!/bin/bash

gapbs=./../gapbs

mkdir -p datasets

NODES="8
       9
       10"

for i in $NODES; do
    $gapbs/converter -u $i -e ./datasets/n$i
done
