#!/usr/bin/python3

import sys

graph_file = sys.argv[1]

print(graph_file)
with open(graph_file, "r") as f:
    cur_node_index = 0
    nodes = []
    edges = []
    for line in f:
        node, edge = [int(i) for i in line.split()]
        print(node, edge)
        if cur_node_index < node:
            cur_node_index += 1
            nodes.append(edges)
            edges = []
            # Save node logic
        edges.append(int(edge))
    nodes.append(edges)

    print("Nodes")
    print(nodes)
