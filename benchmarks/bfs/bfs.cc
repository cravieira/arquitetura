#include <iostream>
#include <list>
#include <fstream>
#include <assert.h>

#include "m5ops.h"
#include "list.hh"

#ifdef USE_STL
#define LIST std::list<int>
#else
#define LIST List<int>
#endif

class Graph
{
private:
    int V; // Number of vertices

    // Pointer to adjacency list.
    LIST *adj;

public:
    Graph(int V);

    void addEdge(uint64_t v, uint64_t w);

    void BFS(int s);

    void dump_list();
};

Graph::Graph(int V) {
    this->V = V;
    adj = new LIST[V];
}

void Graph::addEdge(uint64_t v, uint64_t w) {
    adj[v].push_back(w);
}

void Graph::BFS(int s) {
    bool *visited = new bool[V];
    for (int i = 0; i < V; i++)
        visited[i] = false;

    std::list<int>queue;

    visited[s] = true;
    queue.push_back(s);

    //List<int>::iterator i;

    while (!queue.empty()) {
        s = queue.front();
        std::cout << s << " ";
        queue.pop_front();

        for (LIST::iterator i = adj[s].begin(); i != adj[s].end(); ++i) {
#ifdef __PREFETCH
            int key = i.copy_key();
            if (!visited[key]) {
                visited[key] = true;
                queue.push_back(key);
            }
#else
            int key = i.copy_key();
            if (!visited[key]) {
                visited[key] = true;
                queue.push_back(key);
            }
            //if (!visited[*i]) {
            //    visited[*i] = true;
            //    queue.push_back(*i);
            //}
#endif
        }
    }
}

void Graph::dump_list() {

    for (int v = 0; v < this->V; v++) {
        for (LIST::iterator i = adj[v].begin(); i != adj[v].end(); ++i) {
            std::cout << std::hex << i.base_addr() << " ";
        }
        std::cout << std::endl;
    }
    std::cout << "Size of list is: " << sizeof(List<int>::Ent) << "\n";
    std::cout << "Size of pointer is: " << sizeof(List<int>::Ent*) << "\n";
    std::cout << "Size of int is: " << sizeof(int) << "\n";
    std::cout << "Size of sum is: " << (sizeof(int)+2*(sizeof(List<int>::Ent*))) << "\n";

}

int read_num_of_nodes(std::ifstream &fs) {
    if(fs.is_open())
    {
      //Got to the last character before EOF
      fs.seekg(-1, std::ios_base::end);
      if(fs.peek() == '\n')
      {
        //Start searching for \n occurrences
        fs.seekg(-1, std::ios_base::cur);
        int i = fs.tellg();
        for(i;i > 0; i--)
        {
          if(fs.peek() == '\n')
          {
            //Found
            fs.get();
            break;
          }
          //Move one character back
          fs.seekg(i, std::ios_base::beg);
        }
      }
      int a, b;
      fs >> a >> b;
      return a+1;
    }
    else
    {
      std::cout << "Could not find end line character" << std::endl;
    }
    return 0;
}

int main(int argc, char** argv) {
    uint64_t a, b;

    std::ifstream file(argv[1]);
    a = read_num_of_nodes(file);
    file.seekg(0, std::ios_base::beg);
    std::cerr << a << " nodes" << std::endl;
    Graph g(a);
    while (file >> a >> b) {
        g.addEdge(a, b);
    }

    int source = 0;
    std::cerr << "Following is Breadth First Traversal "
        << "(starting from vertex " << source << ") \n";
    m5_resetstats();
    g.BFS(0);
    m5_dumpstats();
    std::cout << "\n";

    return 0;
}

