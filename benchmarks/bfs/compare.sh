#!/bin/bash

set -e

datasets=datasets
bin=bin/x86
outputs=outputs

mkdir -p $outputs/stl
mkdir -p $outputs/custom

# Generate outputs
for i in $(ls $datasets/*); do
    echo $i
    file_name=$(basename $i)
    ./$bin/*-stl $i > $outputs/stl/$file_name.out
    ./$bin/*-custom $i > $outputs/custom/$file_name.out
done

# Assert outputs
for i in $(ls $outputs/stl); do
    file_name=$(basename $i)
    cmp -s $outputs/stl/$i $outputs/custom/$i
done
