#include <cstdint>
#include <assert.h>

typedef struct ListEntry
{
    ListEntry* next;
    long int key;
} ListEntry;

template <class T> class ListIter;

template <class T>
class List
{
    public:
        struct Ent {
            Ent *next;
            Ent *prev;
            T key;
        };
        typedef ListIter<T> iterator;
        typedef std::ptrdiff_t difference_type;
        typedef std::size_t size_type;
        typedef T value_type;
        typedef T * pointer;
        typedef T & reference;
    private:
        friend class ListIter<T>;

        class Entry
        {
            Entry *next;
            Entry *prev;
            T key;
            public:
                Entry(T e) { this->key = e; next = nullptr; prev = nullptr; }
                T getKey() { return key; }
                Entry* getNext() { return next; }
                Entry* getPrev() { return prev; }
                void setNext(Entry *e) { this->next = e; }
                void setPrev(Entry *e) { this->prev = e; }
        };
        ListEntry* list;
        Ent *first;
        Ent *last;
        uint64_t size;
    public:
        List() { this->first = nullptr; this->last = nullptr; size = 0; }
        List(ListEntry *list) {
            this->list = list;
        }

        void push_back(T key) {
            Ent *entry = new Ent;
            entry->key = key;

            entry->next = nullptr;
            entry->prev = this->last;
            if (this->size == 0) {
                this->first = entry;
                this->last = entry;
            }
            else {
                this->last->next = entry;
                this->last = entry;
            }

            this->size++;
        }

        Ent *find(T x) {
            long int check = 0;
            Ent *p = this->first;

        #ifdef NO_PREFETCH
            // Normal
            while (p != nullptr) {
                long int key = p->key;
                assert(key == check);
                check++;
                if (p->key == x)
                    break;
                p = p->next;
            }
        #else
            // Prefetch form
            while (p != nullptr) {
                long int key;
                asm volatile (
                "ldcp %[dest], %[off](%[addr])" : [dest] "=r" (key)
                                                : [off] "I" (offsetof(ListEntry, key)),
                                                  [addr] "r" (p)
                );
                assert(key == check);
                check++;
                if (key == x)
                    break;
                p = p->next;
            }
        #endif
            return p;
        }

        // Iterator
        iterator begin() { return iterator(*this, this->first); }
        iterator end() { return iterator(*this, nullptr); }
};

template <class T>
class ListIter
{
private:
    List<T> &myList;
    typename List<T>::Ent *myEntry;
public:
    ListIter(List<T> &l, typename List<T>::Ent *entry) :
        myList(l), myEntry(entry)
    {}

    bool operator!=(const ListIter<T> &i) {
        return this->myEntry != i.myEntry;
    }

    ListIter<T> & operator++() {
        this->myEntry = this->myEntry->next;
        return *this;
    }

    int copy_key() {
#ifdef __PREFETCH
        int key;
        asm volatile (
        "lwcp %[dest], %[off](%[addr])" : [dest] "=r" (key)
                                        : [off] "I" (offsetof(List<int>::Ent, key)),
                                          [addr] "r" (this->myEntry)
        );
        return key;
#else
        return this->myEntry->key;
#endif
    }

    T & operator*() {
        return this->myEntry->key;
    }

    typename List<T>::Ent * base_addr() { return this->myEntry; }
    typename List<T>::Ent * next_addr() { return this->myEntry->next; }

};
