#!/bin/bash

set -e

bin=bin/x86
dump=dump/x86
mkdir -p $bin
mkdir -p $dump

make DEFINE="-D USE_STL"
mv bfs $bin/bfs-x86-stl
mv bfs.dump $dump/bfs-stl.dump

make
mv bfs $bin/bfs-custom
mv bfs.dump $dump/bfs-custom.dump
