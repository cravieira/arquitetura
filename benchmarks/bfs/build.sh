#!/bin/bash

set -e

cc=riscv64-unknown-elf-g++
objdump=riscv64-unknown-elf-objdump
bin=bin/riscv
dump=dump/riscv

mkdir -p $bin
mkdir -p $dump

make DEFINE="-D __PREFETCH" CC=$cc OBJDUMP=$objdump
mv bfs $bin/prefetch
mv bfs.dump $dump/prefetch.dump

make CC=$cc OBJDUMP=$objdump
mv bfs $bin/no-prefetch
mv bfs.dump $dump/no-prefetch.dump
