#!/bin/bash

set -e

results=results

# Values be grepped in every stats file.
# ['<dataset name>']='<name in stats file>'
declare -A headers=(
    ['cycles']='numCycles'
    ['ipc']='ipc'
    ['rr_misses']='ReadReq_mshr_misses::total'
    ['rr_avg_miss_latency']='ReadReq_avg_miss_latency::total'
    ['overall_hits']='dcache.overall_hits::total'
    ['overall_misses']='dcache.overall_misses::total'
    ['demand_accesses']='dcache.demand_accesses::total'
)

# $1: regular expression
# $2: gem5 stats file
function grep_cmd() {
    # Every stats file should two stats dumps. The first dump happens when the
    # benchmark calls m5ops_dumpstats(), and the second is made by gem5 at the
    # end of the simulation. Thus, grep -m 1 to get only the first occurence.
    # TODO: Is it possible that a name in stats file occur only in the second
    # dump?
    grep -m 1 "$1" $2 | awk '{print $2}'
}

# $1: dataset name
# $2: gem5 stats file
function parse_stats_file() {
    local dataset=$1
    local stats=()

    for i in ${headers[@]}; do
        stats+=($(grep_cmd $i $2))
    done
    echo ${stats[@]} |
        awk -v d="$dataset" '{print d, $0}' |
        sed 's/ /,/g' >> $3
}

function parse_benchmark_results() {
    rm -f $1/*.csv
    local benchs=$(ls -d $1/*)

    for b in $benchs; do
        datasets=$(ls -d $b/* | sort -V)
        # Write csv header
        echo 'dataset '${!headers[@]} | sed 's/ /,/g' >> $b.csv
        for d in $datasets; do
            dataset=$(basename $d)
            parse_stats_file $dataset $d/stats.txt $b.csv
        done
    done
}

parse_benchmark_results $results/linked-list
parse_benchmark_results $results/matmul
parse_benchmark_results $results/bfs
