#!/bin/bash

set -ex

. script/parameters.sh

BENCHMARK=matmul
benchs=$(ls -d benchmarks/$BENCHMARK/bin/*/)

function run_gem5() {
    if [[ $2 ]]; then
        local bench=$(basename $1)
        local dataset=$(basename $2)
        ./$GEM5 -re -d results/$BENCHMARK/$bench/$dataset $CONFIG $ARGS -c $2
    fi
}
export -f run_gem5

mkdir -p results/

work=""
for b in $benchs; do
    echo $b
    datasets=$(ls $b*)
    for d in $datasets; do
        work+="$b $d\n"
    done
done

echo -e $work |
    parallel -j$MAX_PROCS --colsep ' ' --halt now,fail=1 "run_gem5 {1} {2}"
