#!/bin/bash

set -e

. script/parameters.sh

export BENCHMARK=bfs
benchs=$(ls benchmarks/$BENCHMARK/bin/riscv/*)
datasets=$(ls benchmarks/$BENCHMARK/datasets/*)

function run_gem5() {
    if [[ $1 ]]; then
        bench=$(basename $1)
        data=$(basename $2)
        ./$GEM5 -re -d results/$BENCHMARK/$bench/$data $CONFIG $ARGS -c $1 -o "$2"
    fi
}
export -f run_gem5

work=''
for b in $benchs; do
    mkdir -p results/bfs/$(basename $b)
    for d in $datasets; do
        work+="$b $d\n"
    done
done

echo -e $work | parallel -j$MAX_PROCS --colsep ' ' --halt now,fail=1 "run_gem5 {1} {2}"
