#!/bin/bash
set -e

MAX_PROCS=5

export GEM5=gem5/build/RISCV/gem5.opt
export CONFIG=gem5/configs/example/se.py
export ARGS="-n 1 --cpu-type=MinorCPU --caches --cpu-clock=1GHz --l1d_size=32kB\
             --output=bench-stdout"
export DEBUG="--debug-flags=PseudoInst"
export BENCHMARK=bfs
