#!/usr/bin/env python3

import sys
import pathlib
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

attributes = {'title': 0, 'ylabel': 1, 'bottom': 2}
plots = {
    'cycles': ['Cycles', 'Cycles', None],
    'ipc': ['IPC', 'IPC', None],
    'rr_misses': ['Read Requests: mshr_misses', 'Misses', None],
    'rr_avg_miss_latency': ['Read Requests: Average Miss Latency', 'Latency', None],
    'overall_misses': ['Cache Misses', 'Misses', 'overall_hits']
}

def plot_simple_bars(names, ys, labels,
        title='',
        ylabel='',
        bottom=None,
        path='plot.pdf'):
    x = np.arange(len(names))
    width = 0.3

    fig, ax = plt.subplots(figsize=[13.0,6.0])

    rects = [None] * len(ys)
    base = -(len(ys)-1) / 2
    for i in range(len(ys)):
        off = (base + i) *width
        if bottom:
            rects[i] = ax.bar(x + off, ys[i], width, label=labels[i]+' misses', bottom=bottom[i])
            rects[i] = ax.bar(x + off, bottom[i], width, label=labels[i]+' hits')
        else:
            rects[i] = ax.bar(x + off, ys[i], width, label=labels[i])

    ax.set_ylabel(ylabel)
    ax.set_title(title)
    ax.set_xticks(x)
    ax.set_xticklabels(names)
    ax.legend()

    fig.tight_layout()
    #plt.show()
    plt.savefig(path)
    plt.close()

def plot_benchmark(dps, labels, path='plots'):
    pathlib.Path(path).mkdir(parents=True, exist_ok=True)
    datasets = dps[0]['dataset'].to_numpy()
    bottoms = []
    for c in plots:
        arr1 = dps[0][c].to_numpy()
        arr2 = dps[1][c].to_numpy()
        ys = [arr1, arr2]
        if plots[c][attributes['bottom']] is not None:
            bottom_column = plots[c][attributes['bottom']]
            bottoms.append(dps[0][bottom_column].to_numpy())
            bottoms.append(dps[1][bottom_column].to_numpy())

        plot_simple_bars(datasets, ys, labels,
            title=plots[c][attributes['title']],
            ylabel=plots[c][attributes['ylabel']],
            bottom=bottoms,
            path=path+'/'+c+'.pdf')

bench='./results/linked-list'
bfs_prefetch = pd.read_csv(bench+'/prefetch.csv')
bfs_normal = pd.read_csv(bench+'/no-prefetch.csv')
dps = [bfs_prefetch, bfs_normal]
labels = ['Prefetch', 'Normal']
plot_benchmark(dps, labels, path='plots/linked-list')

bench='./results/matmul'
bfs_prefetch = pd.read_csv(bench+'/prefetch.csv')
bfs_normal = pd.read_csv(bench+'/normal.csv')
dps = [bfs_prefetch, bfs_normal]
labels = ['Prefetch', 'Normal']
plot_benchmark(dps, labels, path='plots/matmul')

bench='./results/bfs'
bfs_prefetch = pd.read_csv(bench+'/prefetch.csv')
bfs_normal = pd.read_csv(bench+'/no-prefetch.csv')
dps = [bfs_prefetch, bfs_normal]
labels = ['Prefetch', 'Normal']
plot_benchmark(dps, labels, path='plots/bfs')
